#![allow(clippy::needless_return)]

use core::iter::IntoIterator;
use num_traits::{Num, Zero};

/// Geeft de deelrij met de maximale som, van een numerieke rij
///
/// Geïmplementeerd volgens [Kadane’s algoritme](https://quanticdev.com/algorithms/dynamic-programming/kadanes-algorithm/).
/// Heeft een looptijd van \(Θ(n)\) in alle gevallen.
///
/// Geeft terug: een drietal (som deelrij, linkerindex deelrij, rechterindex deelrij).
/// Als de linker- en rechterindex `0` zijn, werd geen deelrij gevonden.
#[allow(dead_code)]
fn max_subarray_kadane<I>(n_number: I) -> (I::Item, usize, usize)
where
    // Generic voor alles waarvan een iterator gemaakt kan worden.
    I: IntoIterator,
    // Generic voor alle numerieke items, en bovendien die waardes bevatten, en bovendien die een partiële orde zijn (op volgorde gezet kunnen worden).
    I::Item: Num + Copy + PartialOrd,
{
    let mut index_left = 0;
    let mut index_left_current = 0;
    let mut index_right = 0;
    let mut sum = Zero::zero();
    let mut sum_current = Zero::zero();
    for (index, item) in n_number.into_iter().enumerate() {
        if sum_current <= Zero::zero() {
            index_left_current = index;
            sum_current = item;
        } else {
            sum_current = sum_current + item;
        }
        if sum_current > sum {
            sum = sum_current;
            index_left = index_left_current;
            index_right = index;
        }
    }
    assert!(index_left <= index_right);
    return (sum, index_left, index_right);
}

#[cfg(test)]
mod tests {
    use super::max_subarray_kadane;

    #[test]
    fn test_max_subarray_kadane_1() {
        let array = [1, -435, 54, 65, -6, 34, 456, 3, 434, 6, 232, -19];
        assert_eq!(max_subarray_kadane(array.iter().copied()), (1278, 2, 10));
    }

    #[test]
    fn test_max_subarray_kadane_2() {
        let array = [3, -435];
        assert_eq!(max_subarray_kadane(array.iter().copied()), (3, 0, 0));
    }

    #[test]
    fn test_max_subarray_kadane_3() {
        let array = [-435, 3];
        assert_eq!(max_subarray_kadane(array.iter().copied()), (3, 1, 1));
    }

    #[test]
    fn test_max_subarray_kadane_4() {
        let array = [-11, 22, 33, -44];
        assert_eq!(max_subarray_kadane(array.iter().copied()), (55, 1, 2));
    }

    #[test]
    fn test_max_subarray_kadane_5() {
        // Tricky: -55 maakt som deelrij 1..2 weer 0.
        let array = [-11, 22, 33, -55, 0, 1, 1, 33, 44, -44];
        assert_eq!(max_subarray_kadane(array.iter().copied()), (79, 5, 8));
    }

    #[test]
    fn test_max_subarray_kadane_6() {
        let array = [-11, 22, 33, -55, 0, 1];
        assert_eq!(max_subarray_kadane(array.iter().copied()), (55, 1, 2));
    }
}
